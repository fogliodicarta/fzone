var body;
var tb;
var lunghezzaTb=15;
var tabColori
var colori = ["#009B48","#B90000","#0045AD","#FF5900","#FFFFFF","#FFD500"];
var matr;
var selected;
var iniziale;
var mosse;
var textScreen;
var hero;
var nvb;
var specialstyle=false;
function vittoria(){
  for (var i = 0; i < lunghezzaTb; i++) {
    for (var j = 0; j < lunghezzaTb; j++) {
       if(matr[0][0]!=matr[i][j]){
         return false;
       }
    }
  }
  return true;
}
function creaTb(){
  tabColori=document.createElement("TABLE");
  tabColori.classList.add("table");
  tabColori.classList.add("is-bordered");
  //tabColori.style.position="fixed";
  tabColori.style.margin="auto";
  let tr = document.createElement("TR");
  for(let j=0;j<6;j++){
    let td = document.createElement("TD");
    td.classList.add(j);
    td.classList.add("colori");
    td.style.backgroundColor=colori[j];
    td.style.border="5px solid hsl(0, 0%, 75%)";
    tr.appendChild(td);
  }
  tabColori.appendChild(tr);
  body.appendChild(tabColori);
  textScreen = document.createElement("DIV");
  textScreen.innerHTML="mosse restanti:"+mosse;
  textScreen.classList.add("screen");
  body.appendChild(textScreen);
  tb = document.createElement("TABLE");
  tb.classList.add("table");
  tb.classList.add("is-bordered");
  tb.classList.add("scacchiera");
  body.appendChild(tb);
  for(let i=0;i<lunghezzaTb;i++){
    let tr = document.createElement("TR");
    for(let j=0;j<lunghezzaTb;j++){
      let index =  Math.floor(Math.random() * (5 + 1));
      let td = document.createElement("TD");
      td.classList.add("main-tile");
      if(specialstyle){
        td.innerHTML=index%2==0?'0':'1';
      }else{
        td.innerHTML=" ";
      }
      td.style.backgroundColor=colori[index];
      matr[j][i]=colori[index];
      tr.appendChild(td);
    }
    tb.appendChild(tr);
  }
  textScreen.style.color=matr[0][0];
};
function recursMove(x,y){
  if(x+1<lunghezzaTb&&(iniziale==matr[x+1][y])){
    matr[x][y]=colori[selected];
    recursMove(x+1,y);
  }
  if(y+1<lunghezzaTb&&(iniziale==matr[x][y+1])){
    matr[x][y]=colori[selected];
    recursMove(x,y+1);
  }
  if(x-1>=0&&(iniziale==matr[x-1][y])){
    matr[x][y]=colori[selected];
    recursMove(x-1,y);
  }
  if(y-1>=0&&(iniziale==matr[x][y-1])){
    matr[x][y]=colori[selected];
    recursMove(x,y-1);
  }
  matr[x][y]=colori[selected];
  tb.childNodes[y].childNodes[x].style.backgroundColor=colori[selected]
}
window.onload=function(){
   nvb = document.getElementsByClassName('navbar-item');
   hero = document.getElementById('principale');
  document.getElementById('bulma').onclick = function(){
    colori = ["#ff66ff","#f5f5f5","#ff3860","#23d160","#209cee","#ffdd57"];
    for (var i = 0; i < nvb.length; i++) {
      nvb[i].style.backgroundImage = 'url("bulma.png")';
    }
    hero.style.backgroundImage = 'url("bulma.png")';
  }
  document.getElementById('vivace').onclick = function(){
    colori = ["fuchsia","#e6e6e6","red","lime","blue","yellow"];
    for (var i = 0; i < nvb.length; i++) {
      nvb[i].style.backgroundImage = 'url("vivace.png")';
    }
    hero.style.backgroundImage = 'url("vivace.png")';
  }
  document.getElementById('rubik').onclick = function(){
    colori = ["#009B48","#B90000","#0045AD","#FF5900","#FFFFFF","#FFD500"];
    for (var i = 0; i < nvb.length; i++) {
      nvb[i].style.backgroundImage = 'url("rubik.png")';
    }
    hero.style.backgroundImage = 'url("rubik.png")';
  }
  document.getElementById('estate').onclick = function(){
    colori = ["#37CACE","#FFFE6F","#e9acec","#00008b","#ff5c33","#4dff4d"];
    for (var i = 0; i < nvb.length; i++) {
      nvb[i].style.backgroundImage = 'url("estate.png")';
    }
    hero.style.backgroundImage = 'url("estate.png")';
  }
  document.getElementById('matrix').onclick = function(){
    colori = ["#0D0208","ffffff","#008F11","#00FF41","#BCC6CC","#4d4d4d"];
    specialstyle=true;
    for (var i = 0; i < nvb.length; i++) {
      nvb[i].style.backgroundImage = 'url("matrix.png")';
    }
    hero.style.backgroundImage = 'url("matrix.png")';
  }
  document.getElementById('autunno').onclick = function(){
    colori = ["#391756","#ff6600","#331a00","#009900","#970c1c","#DBA72E"];
    for (var i = 0; i < nvb.length; i++) {
      nvb[i].style.backgroundImage = 'url("autunno.png")';
    }
    hero.style.backgroundImage = 'url("autunno.png")';
  }
  document.getElementById('ez').onclick = function(){
    lunghezzaTb=10;
    inizio();
  }
  document.getElementById('med').onclick = function(){
    lunghezzaTb=15;
    inizio();
  }
  document.getElementById('hrd').onclick = function(){
    lunghezzaTb=20;
    inizio();
  }
};
function inizio(){
  body=document.getElementsByTagName('body')[0];
  body.innerHTML="";
  matr = new Array(lunghezzaTb);

  for (var i = 0; i < matr.length; i++) {
    matr[i] = new Array(lunghezzaTb);
  }
  mosse=30;
  creaTb();
  tabColori.onclick = function(event){
    selected=event.target.classList.item(0);
    if(colori[selected]!=matr[0][0]){
      iniziale=matr[0][0];
      recursMove(0,0);
      textScreen.style.color=matr[0][0];
      let vit=vittoria();
      if(vit || mosse == 0){
          textScreen.innerHTML=vit?"Complimenti hai vinto in "+ (30-mosse) +" mosse!":"Hai finito le mosse, mi dispiace!"
          textScreen.innerHTML += "<br> <p id=\"ricarica\"> Rigioca <i class=\"fas fa-redo-alt\"></i></p>"
          let ricarica=document.getElementById('ricarica')
          tabColori.onclick ={};
          ricarica.onclick=function(){
            location.reload(true);
          };
      }else{
      mosse = mosse - 1;
      textScreen.innerHTML="mosse restanti:"+mosse;
      }
    }
  }
}
