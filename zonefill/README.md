
**Fillzone:**

-------------------

****

**Idea**
====

Nel mio codice HTML ho messo il menu iniziale dove si pu� scegliere la difficolt� di gioco, sono presenti 3 difficolt�:

* Facile: tabella di gioco 10x10, il gioco va completato entro 30 mosse

* Normale: tabella di gioco 15x15, il gioco va completato entro 30 mosse

* Difficile: tabella di gioco 20x20, il gioco va completato entro 30 mosse


Quando viene selezionata la difficolt� di gioco viene chiamata la funzione `inizio()` la quale rimuove il menu di scelta difficolt� e crea una `table` utilizzando `createElement()` e `appendChild()` di javascript poi ad ogni click sulla palette dei colori in alto (`tabColori`) il primo elemento in alto a sinistra nella tabella di gioco viene cambiato nel colore cliccato ed ogni elemento a lui adiacente viene cambiato se era di colore uguale al momento del click.
Per fare ci� utilizza una funzione ricorsiva `recursMove(x,y)` nella quale x � la colonna e y la riga la funzione controlla che i 4 (al massimo) elementi adiacenti abbiano lo stesso colore e richiama se stessa sugli elementi adiacenti che rispettano la condizione.

Dopodich� viene controllata la condizione di vittoria(`vittoria()`) o di perdita(`mosse==0`) , se la tabella � composta esclusivamente da elementi dello stesso colore allora il giocatore ha vinto, se il giocatore ha esaurito le 30 mosse a sua disposizione ha perso, se nessuna di queste condizioni si verifica il giocatore pu� nuovamente cliccare su `tabColori` e continuare la partita.

Al momento di vittoria/perdita viene visualizzato un messaggio che indica in quante mosse � stata vinta la partita o indica la fine delle mosse.

Viene inoltre visualizzato un messaggio con scritto **rigioca!** che quando cliccato riporta alla scelta della difficolt�.

****

**Temi**
====

Ho creato vari temi di colori e sfondi per il gioco ognuno diverso dall'altro:
* bulma: utilizza i colori standard del framework bulma, i colori sono chiari
* vivace: utilizza colori molto vivaci ed accesi
* rubik: utilizza i classici colori del cubo di rubik
* estate: utilizza colori estivi molto chiari
* autunno: utilizza i colori caratteristici dell'autunno come arancione, marrone e porpora
* matrix: tema ispirato ai colori del film The Matrix, verde e nero, dentro le celle sono presenti gli 0 e 1 del codice binario di Matrix
![](https://i.imgur.com/lkbBM56.jpg)*Collage di tutti i temi implementati*
****


Note
====

* � stato utilizzato il framework **Bulma** e l'icon pack **FontAwesome5** sono stati importati nel progetto tramite cdn quindi � importante essere connessi ad internet per visualizzare bene il progetto.

* La scelta di 10x10, 15x15 e 20x20 per le varie difficolt� � stata arbitraria in quanto la funzione javascript `creaTb()` accetta utilizza la variabile `lunghezzaTb` per determinare quante celle creare e quindi avrei potuto creare una tabella di qualsiasi dimensione.

* Ho utilizzato una matrice `matr[lunghezzaTb][lunghezzaTb]` per salvare i colori della tabella per non utilizzare il Dom di html per la ricorsione che sarebbe risultato in un rallentamento del sito.
